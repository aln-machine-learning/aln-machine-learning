// OptionsDialog.cpp : implementation file
//
// Copyright (C) 1995 - 2010 William W. Armstrong.
// 
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
// 
// For further information contact 
// William W. Armstrong
// 3624 - 108 Street NW
// Edmonton, Alberta, Canada  T6J 1B4


#include "stdafx.h"
#include "ALNfitPro.h"
#include "ALNfitProDoc.h"
#include "ALNfitProView.h"
#include "OptionsDialog.h"
#include "DataFile.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



/////////////////////////////////////////////////////////////////////////////
// COptionsDialog dialog


COptionsDialog::COptionsDialog(CWnd* pParent /*=NULL*/)
	: CDialog(COptionsDialog::IDD, pParent)
  , m_strSetTolerance(_T(""))
{
	//{{AFX_DATA_INIT(COptionsDialog)
	m_strTestFileName = _T("");
	m_bDiagnostics = FALSE;
	m_nPercentForTest = 0;
  m_nUseDataFile4Val = 0;
  m_strSetTolerance = _T("");
	m_strValidationFileName = _T("");
	m_nJitter = -1;
	m_nOneLayerDTREE = -1;
	m_nUseDataFile4Val = -1;
	m_nTestOnData = -1;
	m_bTimePrefixes = TRUE;
	m_bReplaceUndefined = FALSE;
	m_bTimePrefixes = FALSE;
	m_bDiagnostics = FALSE;
	m_nDTREEDepth = 0;
	//}}AFX_DATA_INIT
}


void COptionsDialog::DoDataExchange(CDataExchange* pDX)
{
  CDialog::DoDataExchange(pDX);
  //{{AFX_DATA_MAP(COptionsDialog)
  DDX_Text(pDX, IDC_EDITTESTFILENAME, m_strTestFileName);
  DDX_Check(pDX, IDC_CHECK_DIAGNOSTICS, m_bDiagnostics);
  DDX_Text(pDX, IDC_EDITPERCENTFORTEST, m_nPercentForTest);
  DDX_Text(pDX, IDC_EDITVALFILE, m_strValidationFileName);
  DDX_Radio(pDX, IDC_RADIONOJITTER, m_nJitter);
  DDX_Radio(pDX, IDC_RADIOVALUSEDATA, m_nUseDataFile4Val);
  DDX_Text(pDX, IDC_EDIT_TOLERANCE, m_strSetTolerance);
  DDX_Radio(pDX, IDC_RADIOTESTONDATA, m_nTestOnData);
  DDX_Check(pDX, IDC_CHECK_REPLACE_DATA, m_bReplaceUndefined);
  DDX_Check(pDX, IDC_CHECK_TIME_PREFIXES, m_bTimePrefixes);
  DDX_Text(pDX, IDC_EDITDEPTH, m_nDTREEDepth);
  DDV_MinMaxInt(pDX, m_nDTREEDepth, 1, 25);
  //}}AFX_DATA_MAP
  DDX_Text(pDX, IDC_EDIT_TOLERANCE, m_strSetTolerance);
	DDV_MaxChars(pDX, m_strSetTolerance, 20);
}


BEGIN_MESSAGE_MAP(COptionsDialog, CDialog)
	//{{AFX_MSG_MAP(COptionsDialog)
	ON_BN_CLICKED(IDC_BROWSETEST, OnBrowsetest)
	ON_BN_CLICKED(IDC_BROWSEVALIDATION, OnBrowsevalidation)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// COptionsDialog message handlers

void COptionsDialog::OnBrowsetest() 
{
	CFileDialog dlg(TRUE,"txt","*.txt");
	CFileException e;
	CFile file;
  if(dlg.DoModal() ==IDOK)
	{
		BOOL bSuccess;
    bSuccess = file.Open(dlg.GetPathName(),CFile::modeRead, &e);
		if(bSuccess)
    {
      // now we get the test file name from the dialog
			m_strTestFileName = dlg.GetPathName();
      CEdit* pEdit = (CEdit* ) GetDlgItem(IDC_EDITTESTFILENAME);
	    pEdit->SetWindowText(m_strTestFileName);
      m_nTestOnData = 1;
      m_nPercentForTest = 0; // this can also be set to zero independently if no testing is to be done
      file.Close(); // just testing the first time
    }
    else
    {
      MessageBox("Can't open file");
    }
    UpdateData(FALSE);
  }
}

void COptionsDialog::OnBrowsevalidation() 
{
	CFileDialog dlg(TRUE,"txt","*.txt");
	CFileException e;
	CFile file;
  if(dlg.DoModal() ==IDOK)
	{
		BOOL bSuccess;
    bSuccess = file.Open(dlg.GetPathName(),CFile::modeRead, &e);
		if(bSuccess)
    {
      file.Close(); // just testing the first time
      // now we get the test file name from the dialog
			m_strValidationFileName = dlg.GetPathName();
      CEdit* pEdit = (CEdit* ) GetDlgItem(IDC_EDITVALFILE);
	    pEdit->SetWindowText(m_strValidationFileName);
      m_nUseDataFile4Val = 1;
      UpdateData(FALSE);
    }
        else
    {
      MessageBox("Can't open file");
    }
  }
}

BOOL COptionsDialog::OnInitDialog() 
{

  return CDialog::OnInitDialog(); 
}



// This keeps the tolerance input able to be converted to a double
void COptionsDialog::OnEnChangeEditTolerance(void)
{
  double dblTest;
  if((dblTest = atof(LPCTSTR(m_strSetTolerance))) <= 0.0 )
  {
    // the value for tolerance is not acceptable, and we show erase the box
    m_strSetTolerance = "0.5";
    UpdateData(FALSE);
  }
  // if it is acceptable, there is nothing to do

}
