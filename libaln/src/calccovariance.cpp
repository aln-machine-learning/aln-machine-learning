// ALN Library
// Copyright (C) 1995 - 2010 William W. Armstrong.
// 
// This library is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public
// License as published by the Free Software Foundation; either
// version 2.1 of the License, or (at your option) any later version.
// 
// This library is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
// Lesser General Public License for more details.
// 
// You should have received a copy of the GNU Lesser General Public
// License along with this library; if not, write to the Free Software
// Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
// 
// For further information contact 
// William W. Armstrong

// 3624 - 108 Street NW
// Edmonton, Alberta, Canada  T6J 1B4

// calccovariance.cpp

///////////////////////////////////////////////////////////////////////////////
//  File version info:
// 
//  $Archive: /ALN Development/libaln/src/calccovariance.cpp $
//  $Workfile: calccovariance.cpp $
//  $Revision: 7 $
//  $Date: 10/30/07 11:47p $
//  $Author: Arms $
//
///////////////////////////////////////////////////////////////////////////////

#ifdef ALNDLL
#define ALNIMP __declspec(dllexport)
#endif

#include <aln.h>
#include <alnpriv.h>
#include <vector>
#include <ql/math/matrixutilities/svd.hpp>
#include <ql/math/matrix.hpp>

#define TOL 1.0e-12

using namespace std;
using namespace QuantLib;

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif
// helpers for filling and dumping QuantLib matrices
void fillRMA(QuantLib::Matrix& X, double* adblX);
void ALNAPI dumpRMA(const QuantLib::Matrix& X, double* adblX);
void ALNAPI fillCMA(QuantLib::Matrix& X, double* adblX);
void ALNAPI dumpCMA(const QuantLib::Matrix& X, double* adblX);

void fillRMA(QuantLib::Matrix& X, double* adblX)
{
	// The array adblX is organized in row-major order
	QuantLib::Size sRows, nr, sCols, nc;
	sRows = X.rows();
	sCols = X.columns();
	for(nr = 0; nr < sRows; ++nr)
	{
		for(nc = 0; nc < sCols; ++nc)
		{
			X[nr][nc] = adblX[nr*sCols + nc];
		}
	}
}

void ALNAPI dumpRMA(const QuantLib::Matrix& X, double* adblX)
{
	// The array adblX is organized in row-major order
	QuantLib::Size sRows, sr, sCols, sc;
	sRows = X.rows();
	sCols = X.columns();
	for(sr = 0; sr < sRows; ++sr)
	{
		for(sc = 0; sc < sCols; ++sc)
		{
			adblX[sr*sCols + sc] = X[sr][sc] ;
		}
	}
}
/*
void ALNAPI fillCMA(QuantLib::Matrix& X, double* adblX)
{
	// The array adblX is organized in column-major order
	QuantLib::Size sRows, nr, nCols, nc;
	sRows = X.rows();
	nCols = X.columns();
	for(nc = 0; nc < nCols; ++nc)
	{
		for(nr = 0; nr < sRows; ++nr)
		{
			X[nr][nc] = adblX[nc*sRows + nr];
		}
	}
}
void ALNAPI dumpCMA(const QuantLib::Matrix& X, double* adblX)
{
	// The array adblX is organized in column-major order
	QuantLib::Size sRows, nr, nCols, nc;
	sRows = X.rows();
	nCols = X.columns();
	for(nr = 0; nr < sRows; ++nr)
	for(nc = 0; nc < nCols; ++nc)
	{
		for(nr = 0; nr < sRows; ++nr)
		{
			adblX[nc*sRows + nr] = X[nr][nc] ;
		}
	}
}
*/ 
// commented out until we get linking correct

// implementation based on SVD in Quantlib
// note that matrices are stored in row-major order (RMA)
BOOL ALNAPI CalcCovariance(int nCols, // number of input vars = number of ALN inputs - 1
                    int nRows,        // number of rows, i.e. input vectors for a given LFN
                    double* adblX,    // RMA based input vectors (nRows * nCols)
                    double* adblY,    // RMA based result vector (nRows)
                    double* adblC,    // RMA covariance matrix (nCols * nCols)
                    double* adblA,    // RMA fitted parameter vector (nCols)
                    double* adblS,    // RMA std dev vector (nRows) (a weighting on points) 
                    double* adblU,    // RMA U matrix (nRows * nCols)
                    double* adblV,    // RMA V matrix (sCols * nCols)
                    double* adblW,    // RMA W array (nCols)containing singular values
                    double& dblChiSq) // chi square of fit
{
  ASSERT(adblX && adblY && adblC && adblA && adblS && adblU && adblV && adblW);
  BOOL bSuccess = TRUE;
	// Note: if nCols > nRows, then the results are meaningless for ALN approximation
	// so we need some code to highlight this situation
	// calc covariance
	Size sRows = (Size) nRows;
	Size sCols = (Size) nCols;
	Matrix X(sRows,sCols);
	fillRMA(X,adblX); 
	Array Y(sRows,0);
	Array Dummy(sRows,0.0);
	double see;
	for(Size sr = 0; sr < sRows; sr++)
	{
		Y[sr] = adblY[sr];
		see = Y[sr];
	}
	Matrix C(sCols,sCols,0.0);
	Array A(sCols);
	// We should solve for fitting parameters A then compare it to the ALNresult
	// We get A = V W^(-1)U^T Y.  This works for square X but here
	// we would get UWV^TA = UWV^TVW^(-1)U^T Y =Y except for the zeroed  W entries
	Matrix UT(sCols,sRows,0.0);
	Matrix U;
	Matrix V;
	Array  s(sCols,1.0);  // the singular values s
	Array  sinv(sCols,1.0);  // inverses of the singular values s, but inverse of a very small s is 0
	try
  {
		int j;
		double smax,tmp,thresh,sum;
		// in a try/catch block to free b in case exception occurs
		// now we do the SVD on X
		SVD svd(X);
		U = svd.U();
		V = svd.V();
		s = svd.singularValues();
		// put result of the SVD into the arrays in the call
		dumpRMA(V,adblV);
		dumpRMA(U,adblU);
		// find the maximal singular value
		smax=0.0;
		for (j = 0;j < nCols;j++)
			if (s[j] > smax) smax = s[j];
		// if the (non-negative) singular value is less than a threshold, set it to zero
		thresh=TOL*smax;
		for (j  = 0; j < nCols; j++)
		{
			if (s[j] < thresh)
			{
				s[j] = 0.0;
				sinv[j]=0.0;  // surprising but the right way to do it
			}
			else
			{
				sinv[j] = 1.0/s[j];
			}
		}
		// compute the fitting parameters
		UT = transpose(U);
		Dummy = UT * Y;
		Dummy = sinv * Dummy;
		A = V * Dummy;
		// return the values in A to the pointer structure
		for (Size i = 0; i < sCols; i++)
		{
			adblA[i] = A[i];
		}
		// finding the measure of error of the fit by chi-squared 
		dblChiSq=0.0;
		for (Size i = 0; i < sRows; i++) 
		{
			sum=0.0;
			for (Size j = 0; j < sCols; j++)
			{
				sum += X[i][j]* A[j];
			}
			tmp=(Y[i] - sum)/adblS[i]; // adblS[i] is a weight on input vector i, usually 1.0
			dblChiSq += tmp*tmp;
		}

    // calc covariance matrix
		for(Size k = 0; k < sCols; k++)
		{
			for(Size i = 0; i <= k; i++)		// use symmetry
			{
				double sum = 0.0;
				for(Size j = 0; j < sCols; j++)
				{
					sum += V[k][j] * V[i][j] * sinv[j] * sinv[j];
				}
				adblC[i * nCols + k] = adblC[k * nCols + i] = sum; 
			}
		}
  }
  catch(string SVDdecomposition)
  { 
    bSuccess = FALSE; // something failed!
  }  
  return bSuccess;
}
