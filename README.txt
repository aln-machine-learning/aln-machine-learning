This C and C++ software consists of an SDK, the Dendronic Learning Engine, and an application for MS Windows called ALNfit Pro. One goal of this project is to change the GUI from one using MFC (Microsoft Foundation Classes), to one which runs on more platforms (through use of Qt or Mono for example). Everything is released under the LGPL open source license.

The ALNfit Pro application is designed to allow users to test this type of machine learning on some of their own data.  The data can be a tab-separated text file with integers, floats and doubles arranged in several columns.  The learning can be applied to any set of the columns with any set of lags prescribed if the data is a time-series.  The default is to use all columns with no lags as input variables, except for the rightmost column which gives the desired output values.

FEATURES:

The attractive features of the ALN approach to creating smooth non-linear approximations from data samples are:

1. The user doesn't have to pick out a network architecture or a learning rate or any other parameters. Given a data file with the input and output variables indicated, ALNfit Pro first determines the level of noise in the data and then creates an approximation that avoids overfitting. The result is a network consisting of affine functions (e.g. z=ax+by+c), maximum nodes and minimum nodes.

2. The learning algorithm solves the "credit assignment problem", i.e. in an elegant, simple way. For example at a maximum node, the input branch with the greater value holds the parameters to be changed to improve the fit.

3. The learned result can be executed using very simple software at very high speed even without special hardware.

4. The final learned result has a graph consisting of parts of lines, planes (or hyperplanes for more than two inputs). The edges and corners joining the parts are smoothed using polynomials. The smoothed result deviates at most a prescribed amount from the planar pieces, making the smoothed result easy to analyze.

5. A priori knowledge derived from physical or economic principles can be used to constrain the function being learned. For example, it can be forced to be monotonic decreasing in a given variable, or to respect bounds on the partial derivatives.


HISTORY:

The development took place originally at Dendronic Decisions Limited, which was incorporated under Canadian law in 1974. The person mainly responsible for the ALN idea was William W. Armstrong, and the person mainly responsible for earlier versions of its C and C++ code was Monroe Thomas.  The person entirely responsible for design and programming of ALNfit Pro was William W. Armstrong.

The commercial software was used as the basis of several projects for electrical power companies and gas distribution companies, in particular for forecasting the production of power from wind turbines in Europe.  It was also used free of charge for many university research projects, including projects to help in rehabilitation of persons with spinal cord injuries using functional electrical stimulation (FES).

Software which was originally a boolean logic neural net, patented in 1974, was changed to a net using affine (non-homogeneous linear) functions at the leaf nodes and maximum and minimum operators at interior nodes in 1995.  Because of the close relationship between the two types, the name ALN, standing for Adaptive Logic Network, was retained, but all references to the earlier boolean form of ALN machine learning prior to 1995 are now obsolete. The relationship is simple: the form using affine functions at the leaves and maximum and minimum operations is always used for training, but the resulting real function, if compared to a threshold value, is computable by a network consisting of perceptrons and AND and OR gates.

In 2008, the software was changed to open source form under LGPL by vote of the shareholders of Dendronic Decisions Limited.  The company was dissolved, and significant improvements were introduced to the open source form by William W. Armstrong.  A mathematical paper describing ALN networks and the learning algorithm jointly is in preparation by William W. Armstrong and Kenneth O. Cogger (February 2015).

BUILDING THE SOFTWARE:

This version builds in several configurations under Microsoft (R) Visual Studio 2008. 
As the versions of software have changed, you may have to take steps like the following:

The Boost C++ library (http://www.boost.org) must be available.
The current path to it is indicated in libaln/include/ql/qldefines.hpp:

#include <..\..\..\boost_1_54_0\boost\config.hpp>
#include <..\..\..\boost_1_54_0\boost\version.hpp>

For example, you may have version 1.54 of Boost in C:\Boost\boost_1_54_0 and have to add lines
to the Property Pages for both projects libaln and libalndll for all configurations in the
C/C++ Additional Include Directories

C:\Boost\boost_1_54_0\
C:\Boost\boost_1_54_0\boost\
C:\Boost\boost_1_54_0\boost\config

Several routines have been adapted from a version of QuantLib "A free/open-source library for quantitative finance" (http://quantlib.org).
You may use a compiler later than those listed in Quantlib
so you have to change the file relative to win32 above

..\include\ql\config.msvc.hpp

You just copy the latest section, e.g.

#elif(_MSC_VER == 1400)
...
...
down to the 
#else,
and you insert that copied part just above the #else
Then you change the condition in the new part to
#elif(_MSC_VER > 1400)
so there is no error because the program doesn't execute the
else part.  Of course, it is better to actually give the numbers of
successive compilers and the changes necessary for each.
 
ACKNOWLEDGEMENTS:

The process of development of this software goes back to 1967 at Bell Laboratories in Holmdel, New Jersey, where a patent was obtained on a simple learning system.  It continued at the Universit� de Montr�al, during a time when neural nets were undergoing a decline, with the help of Gregor Bochmann, Pierre Cadieux and Jan Gecsei.  High-speed image analysis hardware was designed and an image processing system was built. After 1982, some research on the boolean nets was done at the University of Alberta with the help of Andrew Dwelly and Rolf Manderscheid. ALN software was distributed in the form of a program called Atree. The stimulus to the major change to using linear functions and maximum and minimum operators occurred thanks to the practical demands of a contract from the Defence Research Establishment Suffield under the guidance of Alan McCormack in 1995.  A person who used both types was Aleks Kostov, who applied them to rehabilitation projects.  Further developments took place due to purchases and contracts by Edmonton Power (now Epcor), and in Europe by National Grid Transco Plc and E.on GmbH.  A second contract from DRDC Suffield in 1997 under the direction of Simon Barton and the support of Brant Coughlan and Dmitri Gorodnichy are gratefully acknowledged. There have been several MSc and PhD projects using various versions of the software scattered all over the globe. In particular Olaf Christ and students of Dejan Popovic did thesis work with it.

Among the many persons who have contributed, one person must be given particular credit: Monroe Thomas.  His supreme talent in C and C++ is clear from the structure of the SDK code, of which he was the principal architect up to 1998.
